
CREATE TABLE centre (
	id_centre int4 NOT NULL,
	nom varchar NULL,
	CONSTRAINT centre_pk PRIMARY KEY (id_centre)
);


CREATE TABLE batiment (
	id_batiment int4 NOT NULL,
	nom varchar NULL,
	id_centre int4 NOT NULL,
	CONSTRAINT batiment_pk PRIMARY KEY (id_batiment),
	CONSTRAINT batiment_fk FOREIGN KEY (id_centre) REFERENCES centre(id_centre)
);

CREATE TABLE typeprofil (
	id_type_profil int4 NOT NULL,
	libelle varchar NULL,
	CONSTRAINT typeprofil_pk PRIMARY KEY (id_type_profil)
);

create table roleprofil(
	id_role int4 not null,
	libelle varchar null,
	constraint roleprofil_pk primary key (id_role)
);

CREATE TABLE profil (
	id_profil int4 NOT NULL,
	nom varchar NULL,
	prenom varchar NULL,
	datenaissance date NULL,
	actif boolean NOT NULL,
	mail varchar(255) NULL,
	adresse varchar NULL,
	id_role int4 not NULL,
	id_type_profil int4 not null,
	CONSTRAINT profil_fk FOREIGN KEY (id_role) REFERENCES roleprofil(id_role),
	CONSTRAINT profil_fk2 FOREIGN KEY (id_type_profil) REFERENCES typeprofil(id_type_profil),
	CONSTRAINT profil_pk PRIMARY KEY (id_profil)
);

CREATE TABLE login (
	id_profil int4 NOT NULL,
	login varchar NOT NULL,
	motdepasse varchar NOT NULL,
	CONSTRAINT log_pk PRIMARY KEY (login),
	CONSTRAINT log_fk FOREIGN KEY (id_profil) REFERENCES profil(id_profil)
);

CREATE TABLE typesalle (
	id_type_salle int4 NOT NULL,
	libelle varchar NULL,
	CONSTRAINT typesalle_pk PRIMARY KEY (id_type_salle)
);

CREATE TABLE salle (
	id_salle int4 NOT NULL,
	numero int4 NULL,
	nom varchar NULL,
	capacite int4 NULL,
	nbrchaise int4 NULL,
	nbrtable int4 NULL,
	surface float4 NULL,
	id_batiment int4 NOT NULL,
	id_type_salle int4 NOT NULL,
	CONSTRAINT salle_pk PRIMARY KEY (id_salle),
	CONSTRAINT salle_fk FOREIGN KEY (id_batiment) REFERENCES batiment(id_batiment),
	CONSTRAINT salle_fk_1 FOREIGN KEY (id_type_salle) REFERENCES typesalle(id_type_salle)
);

CREATE TABLE reservation (
	id_reservation int4 NOT NULL,
	id_salle int4 NOT NULL,
	intitul� varchar NULL,
	datedebut date NULL,
	datefin date NULL,
	CONSTRAINT reservation_pk PRIMARY KEY (id_reservation),
	CONSTRAINT reservation_fk FOREIGN KEY (id_salle) REFERENCES salle(id_salle)
);

CREATE TABLE typemateriel (
	id_type_materiel int4 NOT NULL,
	libelle varchar NULL,
	CONSTRAINT typemateriel_pk PRIMARY KEY (id_type_materiel)
);

CREATE TABLE materiel (
	id_materiel int4 NOT NULL,
	quantite int4 NULL,
	id_type_materiel int4 NOT NULL,
	CONSTRAINT materiel_pk PRIMARY KEY (id_materiel),
	CONSTRAINT materiel_fk FOREIGN KEY (id_type_materiel) REFERENCES typemateriel(id_type_materiel)
);

CREATE TABLE archive (
	id_archive int4 NOT NULL,
	nom varchar NULL,
	prenom varchar NULL,
	datenaissance date NULL,
	mail varchar(255) NULL,
	adresse varchar NULL,
	id_role int4 NULL,
	id_type_profil int4 NULL,
	CONSTRAINT archive_pk PRIMARY KEY (id_archive),
	CONSTRAINT archive_fk FOREIGN KEY (id_type_profil) REFERENCES typeprofil(id_type_profil),
	CONSTRAINT archive_fk2 FOREIGN KEY (id_role) REFERENCES roleprofil(id_role)
);

CREATE OR REPLACE function archivage() returns trigger AS $archive$
	begin
		insert into archive(id_archive, nom, prenom, datenaissance, mail, adresse, id_role, id_type_profil) values (nextval('archive_seq'),old.nom, old.prenom, old. datenaissance, old.mail, old.adresse, old.id_role, old.id_type_profil);
		return old;
	end
$archive$ LANGUAGE plpgsql;

CREATE TRIGGER archiver before DELETE ON profil FOR EACH ROW EXECUTE FUNCTION archivage();

INSERT INTO typeprofil(id_type_profil, libelle)VALUES(1, 'Administrateur');
INSERT INTO typeprofil(id_type_profil, libelle)VALUES(2, 'Utilisateur');
INSERT INTO roleprofil(id_role, libelle)VALUES(1, 'Formateur');
INSERT INTO roleprofil(id_role, libelle)VALUES(2, 'Stagiaire');
INSERT INTO typesalle(id_type_salle, libelle)VALUES(1, 'Formation');
INSERT INTO typesalle(id_type_salle, libelle)VALUES(2, 'Bureau');
INSERT INTO typesalle(id_type_salle, libelle)VALUES(3, 'Infirmerie');
INSERT INTO typemateriel(id_type_materiel, libelle)VALUES(1, 'Ordinateur');
INSERT INTO typemateriel(id_type_materiel, libelle)VALUES(2, 'Prise electrique');
INSERT INTO typemateriel(id_type_materiel, libelle)VALUES(3, 'Prise RG45');
INSERT INTO profil VALUES(1, 'Gali', 'Seti', '2020-01-17', true, 'sg@s.g', 'labas', 1, 1);
INSERT INTO login(id_profil, login, motdepasse)VALUES(1, 'admin', 'admin');