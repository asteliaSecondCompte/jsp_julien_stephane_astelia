# jsp_julien_stephane_astelia  

- Trinome :
	- Garcia Stephane  
	- Dorne Julien  
	- Massamba Astelia  

## Configuration  

Créer une base de donnée, aller dans le fichier jspJulienStephaneAstelia\src\main\javahibernate.cfg.xml,  
à la ligne 15,

	<property name="hibernate.connection.url"></property>

ajouter le lien de votre base de donnée

à la ligne 16,

	<property name="hibernate.connection.username"></property>

ajouter l'administrateur

à la ligne 17,

	<property name="hibernate.connection.password"></property>  

ajouter le mot de passe

## Fonctionnalité  

- [ ] Log administrateur  
- [ ] Visualiser tout les utilisateurs  
- [ ] Ajouter un utilisateurs  
- [ ] Modifier un utilisateurs  
- [ ] Ajouter un administrateur  
- [ ] Se déconnecter  
- [ ] Désactiver un utilisateurs  
- [ ] Supprimer un utilisateurs  
