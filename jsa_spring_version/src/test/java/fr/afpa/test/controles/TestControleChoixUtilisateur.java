package fr.afpa.test.controles;

import org.junit.Test;

import fr.afpa.controles.ControleChoixUtilisateur;
import junit.framework.TestCase;

public class TestControleChoixUtilisateur extends TestCase {

	@Test
	public void testVerificationChoix() {
		assertFalse("Probleme controle verification choix", ControleChoixUtilisateur.verificationChoix(null));
		assertFalse("Probleme controle verification choix", ControleChoixUtilisateur.verificationChoix("Bruce"));
		assertTrue("Probleme controle verification choix", ControleChoixUtilisateur.verificationChoix("23"));
	}

}
