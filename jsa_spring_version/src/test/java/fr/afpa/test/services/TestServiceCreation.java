package fr.afpa.test.services;

import org.junit.Test;

import fr.afpa.entites.RolePersonne;
import fr.afpa.services.ServiceCreation;
import junit.framework.TestCase;

public class TestServiceCreation extends TestCase {

	@Test
	public void testConversionRolePersonne() {
		assertTrue("Probleme switch", RolePersonne.STAGIAIRE.equals(new ServiceCreation().conversionRole("stagiaire")));
		assertTrue("Probleme switch", RolePersonne.FORMATEUR.equals(new ServiceCreation().conversionRole("FORMATEUR")));
		assertNull("Probleme switch", new ServiceCreation().conversionRole("manager"));
	}

}
