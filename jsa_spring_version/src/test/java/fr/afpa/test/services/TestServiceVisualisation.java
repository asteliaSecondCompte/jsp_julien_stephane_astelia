package fr.afpa.test.services;



import org.junit.Test;

import fr.afpa.services.ServiceVisualisation;
import junit.framework.TestCase;

public class TestServiceVisualisation extends TestCase {

	@Test
	public void testChaineNonVide() {
		String resultat = "";
		ServiceVisualisation sv = new ServiceVisualisation();
		resultat = sv.afficher();
		assertFalse("Initialisation, Table profil vide", "".equals(resultat));
	}

}
