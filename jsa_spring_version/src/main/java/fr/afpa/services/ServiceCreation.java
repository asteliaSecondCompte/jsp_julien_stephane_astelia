package fr.afpa.services;

import java.time.LocalDate;

import fr.afpa.dto.DTOUtilisateur;
import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Personne;
import fr.afpa.entites.RolePersonne;
import fr.afpa.entites.Utilisateur;

public class ServiceCreation {
	/**
	 * Permet de convertir un role de type String en role de type RolePersonne
	 * 
	 * @param role le role
	 * @return le role
	 */
	public RolePersonne conversionRole(String role) {
		switch (role.toLowerCase()) {
		case "formateur":
			return RolePersonne.FORMATEUR;
		case "stagiaire":
			return RolePersonne.STAGIAIRE;
		default:
			return null;
		}
	}

	/**
	 * Permet de créer une personne
	 * 
	 * @param nom           le nom de la personne
	 * @param prenom        le prenom de la personne
	 * @param dateNaissance la date de naissance de la personne
	 * @param mail          le mail de la personne
	 * @param adresse       l'adresse de la personne
	 * @param role          le role de la personne
	 * @param login         le login de la personne
	 * @param mdp           le mot de passe de la personne
	 * @param admin         le type de personne (true = admin, false = utilisateur)
	 * @return une nouvelle personne
	 */
	public Personne creationPersonne(String nom, String prenom, LocalDate dateNaissance, String mail, String adresse,
			RolePersonne role, String login, String mdp, boolean admin) {
		if (mdp != null) {
			Personne p;
			DTOUtilisateur dtou = new DTOUtilisateur();
			if (admin) {
				p = new Administrateur(nom, prenom, dateNaissance, mail, adresse, true, role);
			} else {
				p = new Utilisateur(nom, prenom, dateNaissance, mail, adresse, true, role);
			}
			dtou.ajoutBDD(p, login, mdp, role);
			return p;
		} else
			return null;
	}
}
