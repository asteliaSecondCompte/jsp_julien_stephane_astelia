package fr.afpa.servlet;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.controles.ControleChoixUtilisateur;
import fr.afpa.dto.DTOUtilisateur;
import fr.afpa.entites.Personne;
import fr.afpa.entites.Utilisateur;
import fr.afpa.services.ServiceModification;
import fr.afpa.services.ServiceVisualisation;

/**
 * Servlet implementation class ServletChoixUser
 */
public class ServletChoixUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletChoixUser() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute(ServletAuthentificationPersonne.AUTHENTIFICATION) != null) {
			DTOUtilisateur dtou = new DTOUtilisateur();
			Map<Integer, Personne> listePersonnes = dtou.listePersonnes();
			if (ControleChoixUtilisateur.verificationChoix(request.getParameter(ServiceModification.CHOIX))) {
				Personne personne = listePersonnes.get(Integer.parseInt(request.getParameter("choix")));
				if (personne instanceof Utilisateur) {
					request.setAttribute("personne", personne);
					request.setAttribute("id", Integer.parseInt(request.getParameter(ServiceModification.CHOIX)));
					request.setAttribute("datenaissance",
							personne.getDateNaissance().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
					RequestDispatcher rd = request.getRequestDispatcher("modifierutilisateur.jsp");
					rd.forward(request, response);
				} else {
					request.setAttribute("choix", ServiceModification.CHOIX);
					request.setAttribute("alluser", new ServiceVisualisation().afficherUser());
					RequestDispatcher rd = request.getRequestDispatcher("choixuser.jsp");
					rd.forward(request, response);
				}
			} else {
				request.setAttribute("choix", ServiceModification.CHOIX);
				request.setAttribute("alluser", new ServiceVisualisation().afficherUser());
				RequestDispatcher rd = request.getRequestDispatcher("choixuser.jsp");
				rd.forward(request, response);
			}
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

}
