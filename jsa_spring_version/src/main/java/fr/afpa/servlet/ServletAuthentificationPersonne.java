package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.controles.ControleAuthentificationUtilisateur;

/**
 * Servlet implementation class ServletAuthentificationPersonne
 */
public class ServletAuthentificationPersonne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String AUTHENTIFICATION = "authentification";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletAuthentificationPersonne() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String mdp = request.getParameter("password");
		ControleAuthentificationUtilisateur cau = new ControleAuthentificationUtilisateur();
		if (cau.controlePersonneInscrite(login, mdp)) {
			request.getSession().setAttribute(AUTHENTIFICATION, true);
			request.getRequestDispatcher("gestionuser.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

}
