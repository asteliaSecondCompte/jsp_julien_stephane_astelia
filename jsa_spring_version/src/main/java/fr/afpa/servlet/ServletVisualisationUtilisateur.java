package fr.afpa.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.entites.Personne;
import fr.afpa.services.ServiceVisualisation;

/**
 * Servlet implementation class ServletVisualisationUtilisateur
 */
public class ServletVisualisationUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletVisualisationUtilisateur() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute(ServletAuthentificationPersonne.AUTHENTIFICATION) != null) {
			ServiceVisualisation sv = new ServiceVisualisation();
			Map<Integer, Personne> listePersonnes = sv.listeTousPersonnes();
			request.getSession(false).setAttribute("listePersonnes", listePersonnes);
			RequestDispatcher rd = request.getRequestDispatcher("visualisationutilisateur.jsp");
			rd.forward(request, response);
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
