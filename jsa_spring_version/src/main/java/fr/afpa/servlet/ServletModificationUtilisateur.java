package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.entites.Personne;
import fr.afpa.entites.Utilisateur;
import fr.afpa.services.ServiceGeneral;
import fr.afpa.services.ServiceModification;

/**
 * Servlet implementation class ServletModificationUtilisateur
 */
public class ServletModificationUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletModificationUtilisateur() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute(ServletAuthentificationPersonne.AUTHENTIFICATION) != null) {
			String req = request.getParameter("modif");
			ServiceModification sm = new ServiceModification();
			switch (req) {
			case "valider":
				if (request.getParameter("password").equals(request.getParameter("password2"))) {
					Personne user = new Utilisateur();
					user.setNom(request.getParameter("nom"));
					user.setPrenom(request.getParameter("prenom"));
					user.setEmail(request.getParameter("mail"));
					user.setAdresse(request.getParameter("adresse"));
					user.setDateNaissance(ServiceGeneral.conversionDate(request.getParameter("datenaissance")));
					sm.modifierUtilisateur(user, Integer.parseInt(request.getParameter("id")),
							request.getParameter("password"));
				}
				break;
			case "desactiver":
				sm.desactiverUtilisateur(Integer.parseInt(request.getParameter("id")));
				break;
			case "supprimer":
				sm.supprimerUtilisateur(Integer.parseInt(request.getParameter("id")));
				break;
			default:
				break;
			}
			request.getRequestDispatcher("Retour").forward(request, response);
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

}
