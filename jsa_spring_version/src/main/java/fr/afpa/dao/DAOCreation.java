package fr.afpa.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Personne;
import fr.afpa.entites.RolePersonne;
import fr.afpa.entitespersistees.LogBDD;
import fr.afpa.entitespersistees.ProfilBDD;
import fr.afpa.entitespersistees.RoleBDD;
import fr.afpa.entitespersistees.TypeProfilBDD;
import fr.afpa.utils.HibernateUtils;

public class DAOCreation {

	/**
	 * 
	 */
	public DAOCreation() {
		super();
	}

	/**
	 * Permet d'enregistrer un utilisateur dans la base de donnee
	 * 
	 * @param profil   le profil de l'utilisateur
	 * @param log      les logs de l'utilisateur
	 * @param role     le role de l'utilisateur
	 * @param personne l'utilisateur
	 * @return true si l'enregistrement est effectuer, false si non
	 */
	public boolean enregistrerUtilisateur(ProfilBDD profil, LogBDD log, RolePersonne role, Personne personne) {
		Session session = HibernateUtils.getSession();
		TypeProfilBDD typeProfil;
		RoleBDD roleBDD;
		// obtention du role
		if ("Formateur".equals(role.getRole())) {
			roleBDD = session.get(RoleBDD.class, 1);
		} else {
			roleBDD = session.get(RoleBDD.class, 2);
		}
		// obtention du type de profil
		if (personne instanceof Administrateur) {
			typeProfil = session.get(TypeProfilBDD.class, 1);
		} else {
			typeProfil = session.get(TypeProfilBDD.class, 2);
		}
		profil.setActif(true);
		profil.setTypeProfil(typeProfil);
		profil.setRole(roleBDD);
		roleBDD.getListeProfils().add(profil);
		Transaction tx = session.beginTransaction();
		session.save(profil);
		session.save(log);
		tx.commit();
		session.close();
		return false;
	}

}
