package fr.afpa.servlet;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.controles.ControleCreationUtilisateur;
import fr.afpa.controles.ControleGeneral;
import fr.afpa.entites.RolePersonne;
import fr.afpa.services.ServiceCreation;
import fr.afpa.services.ServiceGeneral;

/**
 * Servlet implementation class ServletCreationUtilisateur
 */
public class ServletCreationUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletCreationUtilisateur() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute(ServletAuthentificationPersonne.AUTHENTIFICATION) != null) {
			ServiceCreation sc = new ServiceCreation();
			String nom = "";
			String prenom = "";
			String mail;
			String adresse;
			RolePersonne role = RolePersonne.STAGIAIRE;
			LocalDate dateNaissance = LocalDate.now();
			String login = "";
			String password = null;
			RequestDispatcher rq = request.getRequestDispatcher("Retour");
			if (ControleGeneral.controleNomPrenom(request.getParameter("nom"))) {
				nom = request.getParameter("nom");
			}
			if (ControleGeneral.controleNomPrenom(request.getParameter("prenom"))) {
				prenom = request.getParameter("prenom");
			}
			mail = request.getParameter("mail");
			adresse = request.getParameter("adresse");
			if (ControleGeneral.controleRole(request.getParameter("role"))) {
				role = sc.conversionRole(request.getParameter("role"));
			}
			if (ControleGeneral.controleDateDeNaissance(request.getParameter("datenaissance"))) {
				dateNaissance = ServiceGeneral.conversionDate(request.getParameter("datenaissance"));
			}
			if (request.getParameter("password").equals(request.getParameter("password2"))
					&& ControleCreationUtilisateur.controleLogin(request.getParameter("login"))) {
				login = request.getParameter("login");
				password = request.getParameter("password");
			} else {
				request.setAttribute("nom", request.getAttribute("nom"));
				request.setAttribute("prenom", request.getAttribute("prenom"));
				request.setAttribute("mail", request.getAttribute("mail"));
				request.setAttribute("adresse", request.getAttribute("adresse"));
				request.setAttribute("role", request.getAttribute("role"));
				request.setAttribute("datenaissance", request.getAttribute("datenaissance"));
				request.setAttribute("login", request.getAttribute("login"));
				if (ControleCreationUtilisateur.controleLogin(request.getParameter("login")))
					request.setAttribute("existe", false);
				else
					request.setAttribute("existe", true);
				rq = request.getRequestDispatcher("creationutilisateur.jsp");
			}
			if ("user".equals(request.getParameter("create"))) {
				sc.creationPersonne(nom, prenom, dateNaissance, mail, adresse, role, login, password, false);
			} else if ("admin".equals(request.getParameter("create"))) {
				sc.creationPersonne(nom, prenom, dateNaissance, mail, adresse, role, login, password, true);
			}
			rq.forward(request, response);
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

}
