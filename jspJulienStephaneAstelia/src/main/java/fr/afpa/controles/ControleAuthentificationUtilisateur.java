package fr.afpa.controles;

import fr.afpa.dto.DTOUtilisateur;

public class ControleAuthentificationUtilisateur {

	/**
	 * Permet de verifier si une personne est inscrite sur le site
	 * 
	 * @param login : le login a verifier
	 * @param mdp   : le mot de passe a verifier
	 * @return true si la personne est inscrite et false sinon
	 */
	public boolean controlePersonneInscrite(String login, String mdp) {
		DTOUtilisateur dtou = new DTOUtilisateur();
		return dtou.authentificationReussie(login, mdp);
	}

}
