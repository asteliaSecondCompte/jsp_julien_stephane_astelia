package fr.afpa.test.dto;

import org.junit.jupiter.api.Test;

import fr.afpa.dto.DTOGeneral;
import fr.afpa.entites.RolePersonne;
import fr.afpa.entitespersistees.RoleBDD;
import junit.framework.TestCase;

public class TestDTOGeneral extends TestCase {


	@Test
	public void testConversionRolePersonne() {
		RoleBDD role = new RoleBDD("Stagiaire");
		assertEquals("Probleme transformation dao -> metier", DTOGeneral.roleBDDToRolePersonne(role).getRole(), RolePersonne.STAGIAIRE.getRole());
		
		role = new RoleBDD("Formateur");
		assertEquals("Probleme transformation dao -> metier", DTOGeneral.roleBDDToRolePersonne(role).getRole(), RolePersonne.FORMATEUR.getRole());
		
		role = new RoleBDD("Manager");
		assertNull("Probleme transformation dao -> metier", DTOGeneral.roleBDDToRolePersonne(role));
	}

}
