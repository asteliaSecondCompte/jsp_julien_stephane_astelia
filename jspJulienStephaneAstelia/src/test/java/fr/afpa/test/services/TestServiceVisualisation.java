package fr.afpa.test.services;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import fr.afpa.services.ServiceVisualisation;
import junit.framework.TestCase;

public class TestServiceVisualisation extends TestCase {

	@Test
	public void testChaineNonVide() {
		String resultat = "";
		ServiceVisualisation sv = new ServiceVisualisation();
		resultat = sv.afficher();
		assertNotEquals("Initialisation, Table profil vide", "", resultat);
	}

}
